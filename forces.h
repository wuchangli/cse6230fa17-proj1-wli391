#if !defined(FORCES_H)
#define FORCES_H
/* compute repulsive forces for N particles in 3D */
void compute_forces_atomic(double * pos, double krepul, double L, int p, int* pairs, double* forces);
#endif
