a = (3*pi/4*10000/0.2)^(1/3);
fileID = fopen('lac1_novl3.xyz','w');
fprintf(fileID,'%s\n','10000');
fprintf(fileID,'%s %12f\n','0.0',a);
for i = 1:10000
fprintf(fileID,'%d %10.6f %10.6f %10.6f \n', 0, a*rand, a*rand, a*rand);
end
fclose(fileID);