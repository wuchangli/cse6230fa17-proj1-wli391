#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> // access
#include <math.h>
#include <assert.h>
#include <omp.h>
#include "bd.h"
#include "forces.h"


int bd(int npos, double *pos, double L, const int *types, int *maxnumpairs_p, double **dist2_p, int **pairs_p, double* forces)
{
  double f = sqrt(2.*DELTAT);
  
  /* 2 is twice the radius, $2 r_c$ in Prof. Chow's Lecture 5 */
  int     boxdim = L / 2;
  /* Must be at least the square of twice the radius */
  double  cutoff2 = 4.;
  int     maxnumpairs = *maxnumpairs_p;
  double *dist2 = *dist2_p;
  int    *pairs = *pairs_p;
    
  for (int step=0; step<INTERVAL_LEN; step++)
  {
      
    int retval;
    int numpairs = 0;
      
    while (1) {
      retval = interactions(npos, pos, L, boxdim, cutoff2, dist2, pairs, maxnumpairs, &numpairs);
      if (!retval) break;
      if (retval == -1) {
        free(pairs);
        free(dist2);
        maxnumpairs *= 2;
        dist2 = (double *) malloc(maxnumpairs*sizeof(double));
        pairs = (int *) malloc(2*maxnumpairs*sizeof(int));
        assert(dist2);
      } else {
        //return retval;
      }
    }

    double krepul = 100.;
    int p;
    #pragma omp parallel for 
      for (p = 0; p < numpairs; p++) {
      compute_forces_atomic(pos, krepul, L, p, pairs, forces);
    }
      
    // update positions with Brownian displacements
      
//    #pragma omp parallel
//      {
//          cse6230nrand_t crand;
//          unsigned int  seed = 0;
//          
//    #if defined(_OPENMP)
//          seed = omp_get_thread_num();
//    #endif
//          cse6230nrand_seed(seed,&crand);
      
//    #pragma omp for schedule(runtime)
//          for (int i=0; i<3*npos; i++) {
//              double rval = cse6230nrand(nr);
//
//              double noise  = rval;
//              //printf("noise is %f\n", noise);
//              pos[i] += forces[i] * DELTAT + f*noise;
//              forces[i]=0;
//          }
      
//      }
      
    #pragma omp for schedule (static, 10)
    for (int i=0; i<3*npos; i++)
    {
    
      double noise = cse6230nrand(nrand);
       
      pos[i] += forces[i] * DELTAT + f*noise;
      forces[i]=0;
    }
  }
  *maxnumpairs_p = maxnumpairs;
  *dist2_p = dist2;
  *pairs_p = pairs;

  return 0;
}
