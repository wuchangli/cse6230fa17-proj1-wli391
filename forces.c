#include <math.h>
#include <float.h>
#include <stdio.h>
#include <omp.h>
#include "forces.h"

void compute_forces_atomic(double * pos, double krepul, double L, int p, int* pairs, double* forces)
{
    double dx, dy, dz;
    int p1=pairs[2*p], p2=pairs[2*p+1];
    dx = remainder(pos[3*p1+0] - pos[3*p2+0],L);
    dy = remainder(pos[3*p1+1] - pos[3*p2+1],L);
    dz = remainder(pos[3*p1+2] - pos[3*p2+2],L);
          double s2 = dx*dx + dy*dy + dz*dz;
        double s = sqrt(s2);
        double f = krepul*(2.-s);
    #pragma omp atomic
        forces[3*p1+0] += f*dx/s;
    #pragma omp atomic
        forces[3*p1+1] += f*dy/s;
    #pragma omp atomic
        forces[3*p1+2] += f*dz/s;
    #pragma omp atomic
        forces[3*p2+0] -= f*dx/s;
    #pragma omp atomic
        forces[3*p2+1] -= f*dy/s;
    #pragma omp atomic
        forces[3*p2+2] -= f*dz/s;
}


